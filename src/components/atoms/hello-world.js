import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import {setI18nConfig, translate, RNLocalize} from '_utils';

class HelloWorld extends Component {
  constructor(props) {
    super(props);
    setI18nConfig(); // set initial config
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };

  render() {
    const {name} = this.props;
    return (
      <Text>
        {translate('hello')} {name}!{' '}
      </Text>
    );
  }
}

HelloWorld.propTypes = {
  name: PropTypes.string.isRequired,
};

export default HelloWorld;
